#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Cholesky> 


using Eigen::MatrixXd;
using namespace Eigen;
using namespace std; 


int main(int argc, char *argv[]) {

  int taille=atoi(argv[1]);
  srand((unsigned int) time(0)); //So we don't get the same random matrix
  MatrixXd m = MatrixXd::Random(taille,taille); //Random matrix
  MatrixXd b = MatrixXd::Random(taille,taille); //Random matrix
  MatrixXd X = MatrixXd::Zero(taille,taille); //Matrix X initialized with 0.
  MatrixXd Y = MatrixXd::Zero(taille,taille); //Matrix Y initialized with 0.
  MatrixXd A = m*m.transpose();
  
  clock_t tStart = clock();

  MatrixXd L = A.llt().matrixL();
  Y = L.inverse()*b;
  X = L.transpose().inverse()*Y;
  printf("\nTime taken by my implementation: %.2fs\n", (double)(clock() -tStart) / CLOCKS_PER_SEC);
  return 0;
}

//Conreux Jean-Baptiste
//Run icc -Ofast -o opti opti.cpp in the good directory ./opti 1000 as an example for a 1000*1000 matrix.